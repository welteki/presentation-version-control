---

# Version control

<span class="primary"><strong>Han Verstraete and Jonathan Vansteelant</strong></span>
<br>
28.11.2017

---

## Overview
 - What is version control
 - Evolution
 - Concepts and terminology
 - Git demo
 - Conclusion

---

## What is version control?

![version control](/img/version_database.png)

---

## Evolution

+++

## Before version control
Problems:
  - Many duplicate files
  - Who made the change
  - Reason for the change
  - Linear development flow

+++

## SCCS
SCCS stores delta's:
  - Extra information: who, when, where, why

Negative:
  - Only works locally

+++

## RCS

Changes:
  - 'reverse' storing
  - Better interface
  - Branching and merging

Negative:
  - Only works locally

+++

## CVS

Changes:
  - Stored on single server
  - Send notifications

+++

## SVN

Changes:
  - Atomic commits
  - Recognizing renamed/moved files

+++

## DVCS

Changes:
  - Quicker
  - Extra safety

---

## Basic concepts and terminology

![Cartoon](https://www.datamation.com/imagesvr_ce/3829/version-control.jpg)

---

## The repository

---

## Recording changes

---

## Working with remotes

---

## Branching and merging

---

## Short demo
Note: only show if time left

+++

<iframe class="stretch" data-src="https://git-school.github.io/visualizing-git/"></iframe>

---

## Conclusion
When you are working in team or on a larger project,
<br>
**use version control!!!**

---

## Reference
 - [ProGit book](https://git-scm.com/book/nl/v2)
 - [Atlassian Git tutorials](https://www.atlassian.com/git/tutorials/what-is-version-control)
